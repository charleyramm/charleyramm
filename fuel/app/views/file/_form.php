<?php echo Form::open(array("class"=>"form-horizontal",
			    "enctype"=>"multipart/form-data")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Filename', 'filename', array('class'=>'control-label')); ?>
				<?php //echo Form::input('filename', Input::post('filename', isset($file) ? $file->filename : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Filename')); ?>
				<?php echo Form::file(array('name' => 'filename', 'id' => 'filename', 'placeholder' => 'filename')); ?>
		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>
		</div>
	</fieldset>
<?php echo Form::close(); ?>
