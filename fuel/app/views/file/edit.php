<h2>Editing <span class='muted'>File</span></h2>
<br>

<?php echo render('file/_form'); ?>
<p>
	<?php echo Html::anchor('file/view/'.$file->id, 'View'); ?> |
	<?php echo Html::anchor('file', 'Back'); ?></p>
