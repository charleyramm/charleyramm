<h2>Listing <span class='muted'>Files</span></h2>
<br>
<?php if ($files): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Filename</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($files as $item): ?>		<tr>

            <td><a href="/files/<?php echo $item->filename; ?>"><?php echo $item->filename; ?></a></td>
			<td>
				<div class="btn-toolbar">
					<div class="btn-group">
						<?php echo Html::anchor('file/delete/'.$item->id, '<i class="icon-trash icon-white"></i> Delete', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Are you sure?')")); ?>					</div>
				</div>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Files.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('file/create', 'Add new File', array('class' => 'btn btn-success')); ?>

</p>
