<?php
class Controller_File extends Controller_Template
{

	public function action_index()
	{
		$data['files'] = Model_File::find('all');
		$this->template->title = "Files";
		$this->template->content = View::forge('file/index', $data);

	}

	public function action_view($id = null)
	{
		is_null($id) and Response::redirect('file');

		if ( ! $data['file'] = Model_File::find($id))
		{
			Session::set_flash('error', 'Could not find file #'.$id);
			Response::redirect('file');
		}

		$this->template->title = "File";
		$this->template->content = View::forge('file/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST' && Upload::is_valid())
		{
            $fileinfo = Upload::get_files();
                
            $val = Model_File::validate('create');

			if ($val->run(array('filename' => $fileinfo[0]['name'])))
			{
				$file = Model_File::forge(array(
					'filename' => $fileinfo[0]['name'],
				));

				if ($file and $file->save())
				{
                    Upload::save();
					Session::set_flash('success', 'Added file #'.$file->id.'.');

					Response::redirect('file');
				}

				else
				{
					Session::set_flash('error', 'Could not save file.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Files";
		$this->template->content = View::forge('file/create');

	}

	public function action_edit($id = null)
	{
		is_null($id) and Response::redirect('file');

		if ( ! $file = Model_File::find($id))
		{
			Session::set_flash('error', 'Could not find file #'.$id);
			Response::redirect('file');
		}

		$val = Model_File::validate('edit');

		if ($val->run())
		{
			$file->filename = Input::post('filename');

			if ($file->save())
			{
				Session::set_flash('success', 'Updated file #' . $id);

				Response::redirect('file');
			}

			else
			{
				Session::set_flash('error', 'Could not update file #' . $id);
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$file->filename = $val->validated('filename');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('file', $file, false);
		}

		$this->template->title = "Files";
		$this->template->content = View::forge('file/edit');

	}

	public function action_delete($id = null)
	{
		is_null($id) and Response::redirect('file');

		if ($file = Model_File::find($id))
		{
			$file->delete();

			Session::set_flash('success', 'Deleted file #'.$id);
		}

		else
		{
			Session::set_flash('error', 'Could not delete file #'.$id);
		}

		Response::redirect('file');

	}

}
